module.exports = {
  devServer: {
      //port: 8888,
      open: true,
      https: false,
      host: "localhost",
      // proxy: {
      //     [process.env.VUE_APP_BASE_URL]: {
      //         target: process.env.VUE_APP_SERVER_URL,
      //         changeOrigin: true,
      //         pathRewrite: {
      //             ['^' + process.env.VUE_APP_BASE_URL]: ''
      //         }
      //     }
      // }
  },
  lintOnSave: false,
  //     chainWebpack: config => {
  //         config.when(process.env.NODE_ENV === 'production', config => {
  //             config.entry('app').clear().add('./src/main-prod.js')
  //         })
  //         config.when(process.env.NODE_ENV === 'development', config => {
  //             config.entry('app').clear().add('./src/main-dev.js')
  //         })
  //     }
  // 
  productionSourceMap: false
}