import Vue from 'vue'
import App from './App.vue'
import router from './router'


// 导入默认css
import './assets/css/global.css'

// 导入图标字体样式
import './assets/fonts/iconfont.css'
// 导入element css
import 'element-ui/lib/theme-chalk/index.css'
import './plugins/element'

// 导入vue-table-with-tree-grid
import TreeTable from 'vue-table-with-tree-grid'
// 导入axios异步组件
import http from './plugins/http'

// 导入store
import store from './store'

Vue.config.productionTip = false

Vue.prototype.$http = http
Vue.component('tree-table', TreeTable)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
