import axios from 'axios'
import NProgress from 'nprogress'

const http = axios.create({
    // baseURL: process.env.VUE_APP_BASE_URL,
    baseURL: 'http://api.leyou.com/api/item',
    // baseURL: 'http://localhost:9001',
    timeout: 5000
})

//请求拦截器
http.interceptors.request.use(config => {
    // console.log(config);
    // if (config.url === '/sysuser/login') {
    //     config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    // }
    NProgress.start()
    return config
})

// 响应拦截器
http.interceptors.response.use(resp => {
    // console.log(resp);
    if (resp.status >= 200 && resp.status < 300) {
        NProgress.done()
        return resp.data
    }
}, error => {
    console.log(error);
})
export default http