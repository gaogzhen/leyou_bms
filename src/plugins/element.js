import Vue from 'vue'

// elementUI 按需导入
import {
  Header,
  Footer,
  Container,
  Aside,
  Main,
  Form,
  FormItem,
  Input,
  Select,
  Option,
  RadioGroup,
  Radio,
  CheckboxGroup,
  Checkbox,
  Button,
  Table,
  TableColumn,
  Dialog,
  Pagination,
  Message,
  MessageBox,
  Dropdown,
  Avatar,
  DropdownMenu,
  DropdownItem,
  Menu,
  MenuItem,
  Submenu,
  Breadcrumb,
  BreadcrumbItem,
  Card,
  Row,
  Col,
  Tag,
  Cascader,
  Upload,
  Alert,
  Tabs,
  TabPane
} from 'element-ui'

Vue.use(Header)
Vue.use(Footer)
Vue.use(Aside)
Vue.use(Container)
Vue.use(Form)
Vue.use(Main)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Option)
Vue.use(Select)
Vue.use(RadioGroup)
Vue.use(Radio)
Vue.use(CheckboxGroup)
Vue.use(Checkbox)
Vue.use(Button)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Dialog)
Vue.use(Pagination)
Vue.use(Dropdown)
Vue.use(Avatar)
Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Card)
Vue.use(Row)
Vue.use(Col)
Vue.use(Tag)
Vue.use(Cascader)
Vue.use(Upload)
Vue.use(Alert)
Vue.use(Tabs)
Vue.use(TabPane)



const m = {
  info(msg) {
    Message({
      duration: 800,
      message: msg,
      type: 'info'
    });
  },
  error(msg) {
    Message({
      duration: 800,
      message: msg,
      type: 'error'
    });
  },
  success(msg) {
    Message({
      duration: 800,
      message: msg,
      type: 'success'
    });
  },
  warning(msg) {
    Message({
      duration: 800,
      message: msg,
      type: 'warning'
    });
  },
  confirm(msg) {
    return new Promise((resolve, reject) => {
      MessageBox.confirm(msg, '提示', {
          confirmButtonText: '确定',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          resolve()
        })
        .catch(() => {
          reject()
        });
    })
  },
  prompt(msg) {
    return new Promise((resolve, reject) => {
      MessageBox.prompt(msg, '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消'
      }).then(({
        value
      }) => {
        resolve(value)
      }).catch(() => {
        reject()
      });
    })
  }
}

Vue.prototype.$message = m