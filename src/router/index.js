import Vue from "vue";
import VueRouter from "vue-router";
import Home from "@/views/Home.vue";
import Report from "@/views/index/Report.vue";
import Category from "@/views/item/Category.vue";
import Brand from "@/views/item/Brand.vue";
import GoodsList from "@/views/item/GoodsList.vue";
import Specification from "@/views/item/Specification.vue";
import SpecParams from "../views/item/SpecParams.vue";
import SpecGroup from "../views/item/SpecGroup.vue";
import UserList from "@/views/user/UserList.vue";
import Order from "@/views/sale/Order.vue";
import Role from "@/views/authority/Role.vue";

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    redirect: "/report",
    children: [
      {
        path: "/report",
        name: "report",
        component: Report,
      },
      {
        path: "/category",
        name: "category",
        component: Category,
      },
      {
        path: "/brand",
        name: "brand",
        component: Brand,
      },
      {
        path: "/goodsList",
        name: "goodsList",
        component: GoodsList,
      },
      {
        path: "/specification",
        name: "specification",
        component: Specification,
        children: [
          {
            path: "/specParams",
            name: "specParams",
            component: SpecParams,
          },
          {
            path: "/specGroup",
            name: "specGroup",
            component: SpecGroup,
          },
        ],
      },

      {
        path: "/userList",
        name: "userList",
        component: UserList,
      },
      {
        path: "/order",
        name: "order",
        component: Order,
      },
      {
        path: "/role",
        name: "role",
        component: Role,
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
